﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace Morgengry
{
    public class Book
    {
        public string ItemId { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }

        public Book (string itemId, string title, double price)
        {
            this.ItemId = itemId;
            this.Title = title;
            this.Price = price;
        }

        public Book(string itemId, string title)
        {
            this.ItemId = itemId;
            this.Title = title;
        }


        public Book(string itemId)
        {
            this.ItemId = itemId;
        }

        //public Book()
        //{

        //}
        public override string ToString()
        {
            return $"ItemId: {ItemId}, Title: {Title}, Price: {Price}";
        }

    }
}
