﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Morgengry
{
    public class Utility
    {
        public static double GetValueOfBook(Book book)
        {
            double value = book.Price;
            return value;
        }

        public static double GetValueOfAmulet(Amulet amulet)
        {
            double value = 69.420;
            switch (amulet.Quality)
            {
                case Level.low:
                    value = 12.5;
                    break;

                case Level.medium:
                    value = 20.0;
                    break;
                case Level.high:
                    value = 27.5;
                    break;
            }
            return value;
            
        }
    }
}
